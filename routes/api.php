<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get(
    '/',
    function () {
        return response()
            ->json(
                [
                    'message' => 'Imóveis API',
                    'status' => 'Connected',
                ],
                200
            );
    }
);

Route::namespace('Api')->prefix('v1')->group(function () {
    Route::get('properties/select', 'PropertyController@getPropertiesSelect');
    Route::resource('properties', 'PropertyController')->except('create', 'edit');
    Route::resource('lease-agreements', 'LeaseAgreementController')->except('create', 'edit');
});
