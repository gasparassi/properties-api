<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes;

    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_email', 'street', 'number', 'complement', 'neighborhood', 'city', 'state', 'status'
    ];

    /**
     * Define accessor
     *
     * @return void
     */
    public function getStatusAttribute($value)
    {
        return $value ? 'Contratado' : 'Não Contratado';
    }

    /**
     * Define accessor
     *
     * @return void
     */
    public function getFullAddressAttribute()
    {
        return "{$this->street}, {$this->number}, {$this->neighborhood}, {$this->city}, {$this->state}";
    }

    public function leaseAgreement()
    {
        return $this->hasOne(LeaseAgreement::class);
    }
}
