<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaseAgreement extends Model
{
    use SoftDeletes;
    
    protected $table = 'lease_agreements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id', 'person_or_entity', 'cpf_cnpj', 'contractor_email', 'contractor_name',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
    
    /**
     * Define accessor
     *
     * @return void
     */
    public function getPersonOrEntityAttribute($value)
    {
        return $value ? 'Pessoa Física' : 'Pessoa Jurídica';
    }

    /**
     * Define accessor
     *
     * @return void
     */
    public function getCpfCnpjAttribute($value)
    {
        if (strlen($value) === 11) {
            $docFormatado = substr($value, 0, 3) . '.' .
                                substr($value, 3, 3) . '.' .
                                substr($value, 6, 3) . '-' .
                                substr($value, 9, 2);
        } else {
            $docFormatado = substr($value, 0, 2) . '.' .
                            substr($value, 2, 3) . '.' .
                            substr($value, 5, 3) . '/' .
                            substr($value, 8, 4) . '-' .
                            substr($value, -2);
        }
        return $docFormatado;
    }
}
