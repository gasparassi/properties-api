<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;
use App\Rules\RentedProperty;

class LeaseAgreementStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property_id' => ['required', 'integer', 'exists:properties,id', new RentedProperty()],
            'person_or_entity' => ['required', 'boolean',],
            'cpf_cnpj' => ['required', 'string', 'max:15'],
            'contractor_email' => ['required', 'email', 'max:50'],
            'contractor_name' => ['required', 'string', 'max:100'],
        ];
    }

    public function messages()
    {
        return [
            'property_id.exists' => 'O imóvel escolhido não é válido.',
        ];
    }
}
