<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeaseAgreementStoreRequest;
use App\Http\Requests\LeaseAgreementUpdateRequest;
use App\Services\Contracts\LeaseAgreementServiceInterface;

class LeaseAgreementController extends Controller
{

    private $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LeaseAgreementServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->service->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LeaseAgreementStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeaseAgreementStoreRequest $request)
    {
        return $this->service->create($request->validated());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  LeaseAgreementUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LeaseAgreementUpdateRequest $request, $id)
    {
        return $this->service->update($id, $request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}
