<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\PropertyStoreRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropertyUpdateRequest;
use App\Services\Contracts\PropertyServiceInterface;

class PropertyController extends Controller
{

    private $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PropertyServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->service->getAll();
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  PropertyStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PropertyStoreRequest $request)
    {
        return $this->service->create($request->validated());
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PropertyUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PropertyUpdateRequest $request, $id)
    {
        return $this->service->update($id, $request->validated());
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }

    public function getPropertiesSelect()
    {
        return $this->service->getPropertiesForSelect();
    }
}
