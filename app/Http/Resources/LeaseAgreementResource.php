<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LeaseAgreementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'person_or_entity' => $this->person_or_entity,
            'cpf_cnpj' => $this->cpf_cnpj,
            'contractor_email' => $this->contractor_email,
            'contractor_name' => $this->contractor_name,
            'property' => new PropertyResource($this->property),
        ];
    }
}
