<?php

namespace App\Services\Contracts;

use App\Models\Property;
use Illuminate\Http\JsonResponse;

interface PropertyServiceInterface
{
    public function create(array $attributes): ?JsonResponse;
    public function find(int $id): ?JsonResponse;
    public function getAll(): ?JsonResponse;
    public function update(int $id, array $attributes): JsonResponse;
    public function delete(int $id): JsonResponse;
    public function getPropertiesForSelect(): JsonResponse;
}
