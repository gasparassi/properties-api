<?php

namespace App\Services\Contracts;

use Illuminate\Http\JsonResponse;

interface LeaseAgreementServiceInterface
{
    public function create(array $attributes): ?JsonResponse;
    public function find(int $id): ?JsonResponse;
    public function getAll(): ?JsonResponse;
    public function update(int $id, array $attributes): JsonResponse;
    public function delete(int $id): JsonResponse;
}
