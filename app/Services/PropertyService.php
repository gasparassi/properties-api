<?php

namespace App\Services;

use App\Services\Contracts\PropertyServiceInterface;
use Illuminate\Http\Response;
use App\Http\Resources\PropertyResource;
use App\Repositories\Contracts\PropertyRepositoryInterface;
use Illuminate\Http\JsonResponse;

class PropertyService extends BaseService implements PropertyServiceInterface
{

    private $repository;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(PropertyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(array $attributes): JsonResponse
    {
        try {
            $property = $this->repository->createNew($attributes);
            if ($property !== null) {
                return response()->json([
                    'data' => new PropertyResource($property),
                    'status' => Response::HTTP_CREATED,
                ], Response::HTTP_CREATED);
            } else {
                return response()->json([
                    'message' => 'Erro na validação dos dados.',
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function find(int $id): JsonResponse
    {
        try {
            $property = $this->repository->findOne($id);
            if ($property !== null) {
                return response()->json([
                    'data' => $property,
                    'status' => Response::HTTP_OK,
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Imóvel não encontrado com os parâmetros informados.',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function getAll(): JsonResponse
    {
        try {
            $properties = $this->repository->getAll();
            if ($properties->total() > 0) {
                return response()->json([
                    'data' => PropertyResource::collection($properties),
                    'status' => Response::HTTP_OK,
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Nenhum imóvel cadastrado;',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function update(int $id, array $attributes): JsonResponse
    {
        try {
            $property = $this->repository->findOne($id);
            if ($property !== null) {
                $propertyUpdated = $this->repository->updateOne($property, $attributes);

                return response()->json([
                    'data' => new PropertyResource($propertyUpdated),
                    'status' => Response::HTTP_OK,
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Erro ao atualizar o imóvel com os dados informados.',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function delete(int $id): JsonResponse
    {
        try {
            $property = $this->repository->findOne($id);
            if ($property !== null) {
                $propertyDeleted = $this->repository->deleteOne($property);

                if ($propertyDeleted) {
                    return response()->json([
                        'message' => 'Imóvel removido com sucesso.',
                        'status' => Response::HTTP_OK,
                    ], Response::HTTP_OK);
                }
            } else {
                return response()->json([
                    'message' => 'Erro ao remover o imóvel com os dados informados.',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function getPropertiesForSelect(): JsonResponse
    {
        try {
            $properties = $this->repository->getAllPropertiesForSelect();
            if ($properties !== null) {
                return response()->json([
                    'data' => $properties,
                    'status' => Response::HTTP_OK,
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Erro ao remover o imóvel com os dados informados.',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }
}
