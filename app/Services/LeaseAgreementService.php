<?php

namespace App\Services;

use App\Http\Resources\LeaseAgreementResource;
use Illuminate\Http\Response;
use App\Repositories\Contracts\LeaseAgreementRepositoryInterface;
use App\Services\Contracts\LeaseAgreementServiceInterface;
use Illuminate\Http\JsonResponse;

class LeaseAgreementService extends BaseService implements LeaseAgreementServiceInterface
{

    private $repository;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(LeaseAgreementRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(array $attributes): JsonResponse
    {
        try {
            $leaseAgreement = $this->repository->createNew($attributes);
            if ($leaseAgreement !== null) {
                return response()->json([
                    'data' => new LeaseAgreementResource($leaseAgreement),
                    'status' => Response::HTTP_CREATED,
                ], Response::HTTP_CREATED);
            } else {
                return response()->json([
                    'message' => 'Erro na validação dos dados.',
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function find(int $id): JsonResponse
    {
        try {
            $leaseAgreement = $this->repository->findOne($id);
            if ($leaseAgreement !== null) {
                return response()->json([
                    'data' => new LeaseAgreementResource($leaseAgreement),
                    'status' => Response::HTTP_OK,
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Contrato de locação não encontrado com os parâmetros informados.',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function getAll(): JsonResponse
    {
        try {
            $leaseAgreements = $this->repository->getAll();
            if ($leaseAgreements->total() > 0) {
                return response()->json([
                    'data' => LeaseAgreementResource::collection($leaseAgreements),
                    'status' => Response::HTTP_OK,
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Nenhum contrato de locação cadastrado;',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function update(int $id, array $attributes): JsonResponse
    {
        try {
            $leaseAgreement = $this->repository->findOne($id);
            if ($leaseAgreement !== null) {
                $leaseAgreementUpdated = $this->repository->updateOne($leaseAgreement, $attributes);

                return response()->json([
                    'data' => new LeaseAgreementResource($leaseAgreementUpdated),
                    'status' => Response::HTTP_OK,
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Erro ao atualizar o contrato de locação com os dados informados.',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }

    public function delete(int $id): JsonResponse
    {
        try {
            $leaseAgreement = $this->repository->findOne($id);
            if ($leaseAgreement !== null) {
                $leaseAgreementDeleted = $this->repository->deleteOne($leaseAgreement);

                if ($leaseAgreementDeleted) {
                    return response()->json([
                        'message' => 'Contrato de locação removido com sucesso.',
                        'status' => Response::HTTP_OK,
                    ], Response::HTTP_OK);
                }
            } else {
                return response()->json([
                    'message' => 'Erro ao remover o contrato de locação com os dados informados.',
                    'status' => Response::HTTP_NOT_FOUND,
                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->getFormatErrorMessage($th->getMessage());
        }
    }
}
