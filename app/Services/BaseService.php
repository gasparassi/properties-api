<?php

namespace App\Services;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class BaseService
{
    public function getFormatErrorMessage(string $msg): JsonResponse
    {
        return response()->json([
            'error' => 'Erro interno do servidor',
            'message' => $msg,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
