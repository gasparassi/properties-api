<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Contracts\{
    PropertyServiceInterface,
    LeaseAgreementServiceInterface,
};
use App\Services\{
    PropertyService,
    LeaseAgreementService
};

class ModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PropertyServiceInterface::class, PropertyService::class);
        $this->app->bind(LeaseAgreementServiceInterface::class, LeaseAgreementService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
