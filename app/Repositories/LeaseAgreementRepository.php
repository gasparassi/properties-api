<?php

namespace App\Repositories;

use App\Models\LeaseAgreement;
use App\Models\Property;
use App\Repositories\Contracts\LeaseAgreementRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class LeaseAgreementRepository implements LeaseAgreementRepositoryInterface
{

    private $entity;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(LeaseAgreement $leaseAgreement)
    {
        $this->entity = $leaseAgreement;
    }

    public function createNew(array $attributes): ?LeaseAgreement
    {
        $leaseAgreement = $this->entity->create($attributes);

        $leaseAgreement->property->status = true;
        $leaseAgreement->property->save();

        return $leaseAgreement;
    }

    public function findOne(int $id): ?LeaseAgreement
    {
        return $this->entity->where('id', $id)->first();
    }

    public function getAll(): ?LengthAwarePaginator
    {
        return $this->entity->paginate();
    }
    
    /**
     * Atualiza o status da imóvel contratado
     *
     * @param  LeaseAgreement $model
     * @param  bool $value
     * @return void
     */
    private function updateLeaseAgreementProperty(LeaseAgreement $model, bool $value)
    {
        $model->property->status = $value;
        $model->property->save();
    }
    
    public function updateOne(LeaseAgreement $model, array $attributes): ?LeaseAgreement
    {
        if ($model->property->id !== $attributes['property_id']) {
            $this->updateLeaseAgreementProperty($model, false);
        }
        $updated = $model->update($attributes);

        if ($updated) {
            $freshModel = $this->findOne($model->id);
        }

        $this->updateLeaseAgreementProperty($freshModel, true);

        return $freshModel;
    }

    public function deleteOne(LeaseAgreement $model): bool
    {
        return $model->delete();
    }
}
