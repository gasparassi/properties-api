<?php

namespace App\Repositories\Contracts;

use App\Models\Property;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface PropertyRepositoryInterface
{
    public function createNew(array $attributes): ?Property;
    public function findOne(int $id): ?Property;
    public function getAll(): ?LengthAwarePaginator;
    public function updateOne(Property $model, array $attributes): ?Property;
    public function deleteOne(Property $model): bool;
    public function getAllPropertiesForSelect(): ?Collection;
}
