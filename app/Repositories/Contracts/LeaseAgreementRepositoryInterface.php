<?php

namespace App\Repositories\Contracts;

use App\Models\LeaseAgreement;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface LeaseAgreementRepositoryInterface
{
    public function createNew(array $attributes): ?LeaseAgreement;
    public function findOne(int $id): ?LeaseAgreement;
    public function getAll(): ?LengthAwarePaginator;
    public function updateOne(LeaseAgreement $model, array $attributes): ?LeaseAgreement;
    public function deleteOne(LeaseAgreement $model): bool;
}
