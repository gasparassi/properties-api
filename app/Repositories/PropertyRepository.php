<?php

namespace App\Repositories;

use App\Models\Property;
use App\Repositories\Contracts\PropertyRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class PropertyRepository implements PropertyRepositoryInterface
{

    private $entity;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(Property $property)
    {
        $this->entity = $property;
    }

    public function createNew(array $attributes): ?Property
    {
        return $this->entity->create($attributes);
    }

    public function findOne(int $id): ?Property
    {
        return $this->entity->where('id', $id)->first();
    }

    public function getAll(): ?LengthAwarePaginator
    {
        return $this->entity->paginate();
    }

    public function updateOne(Property $model, array $attributes): ?Property
    {
        $updated = $model->update($attributes);

        if ($updated) {
            $freshModel = $this->findOne($model->id);
        }

        return $freshModel;
    }

    public function deleteOne(Property $model): bool
    {
        return $model->delete();
    }

    public function getAllPropertiesForSelect(): ?Collection
    {
        return $this->entity->where('status', false)->get(['id', 'street', 'number', 'complement', 'neighborhood']);
    }
}
